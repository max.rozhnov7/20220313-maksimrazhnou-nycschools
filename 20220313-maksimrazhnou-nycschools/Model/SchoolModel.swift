//
//  SchoolModel.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/14/22.
//

import Foundation

//I'd love to use Core Data instead, but it felt like an overkill for such a small app.
struct SchoolModel {
    let id: String?
    let schoolName: String?
    let email: String?
    let addressLine1: String?
    let city: String?
    let zip: String?
    let state: String?
    let phoneNumber: String?
    let website: String?
    
    let latitude: String?
    let longitude: String?
    
    let criticalReadingAverageScore: Double?
    let mathAverageScore: Double?
    let writingAverageScore: Double?
        
    init(schoolResponseModel: SchoolResponseModel, satScoresResponseModel: SchoolSATScoresResponseModel?) {
        self.id = schoolResponseModel.id
        self.schoolName = schoolResponseModel.schoolName
        self.email = schoolResponseModel.email
        self.addressLine1 = schoolResponseModel.addressLine1
        self.city = schoolResponseModel.city
        self.zip = schoolResponseModel.zip
        self.state = schoolResponseModel.state
        self.phoneNumber = schoolResponseModel.phoneNumber
        self.website = schoolResponseModel.website
        
        self.latitude = schoolResponseModel.latitude
        self.longitude = schoolResponseModel.longitude
        
        self.criticalReadingAverageScore = satScoresResponseModel?.criticalReadingAverageScore
        self.mathAverageScore = satScoresResponseModel?.mathAverageScore
        self.writingAverageScore = satScoresResponseModel?.writingAverageScore
    }
}

extension SchoolModel {
    var address: String? {
        guard let addressLine1 = addressLine1,
              let city = city
            else {
                return nil
            }
        return "\(addressLine1), \(city), \(state ?? "NY")"
    }
}

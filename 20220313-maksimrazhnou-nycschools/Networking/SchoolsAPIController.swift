//
//  SchoolsAPIController.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

import Foundation

/*
 I'm very used to using Pods like Alamofire or Moya for networking so
 building this small APIController from scratch was fun and challenging.
*/
class SchoolsAPIController: APIController {
    /*
     I'd probably like to look further into this function, as I don't like the completion closure
      having an array of errors as a parameter.
     */
    func fetchSchoolsData(completion: @escaping ([SchoolModel]?, [Error]?) -> Void) {
        let dispatchGroup = DispatchGroup()
        
        var schools = [SchoolResponseModel]()
        var satScores = [SchoolSATScoresResponseModel]()
        
        var errors = [Error]()
        
        dispatchGroup.enter()
        fetchSchools { fetchedSchools, error in
            DispatchQueue.main.async {
                if let error = error {
                    errors.append(error)
                }
                schools = fetchedSchools ?? []
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.enter()
        fetchSATScores(completion: { fetchedScores, error in
            DispatchQueue.main.async {
                if let error = error {
                    errors.append(error)
                }
                satScores = fetchedScores ?? []
                dispatchGroup.leave()
            }
        })
        
        dispatchGroup.notify(queue: .main) {
            var schoolsData = [SchoolModel]()
            for school in schools {
                schoolsData.append(SchoolModel(schoolResponseModel: school,
                                               satScoresResponseModel: satScores.first { $0.id == school.id }))
            }
            if !errors.isEmpty {
                completion(schoolsData, errors)
            } else {
                completion(schoolsData, nil)
            }
        }
    }
    
    private func fetchSchools(completion: @escaping ([SchoolResponseModel]?, Error?) -> Void) {
        if let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") {
            fetchAndDecodeData(from: url, completion: completion)
        } else {
            completion(nil, NetworkingError.invalidUrl)
        }
    }
    
    private func fetchSATScores(completion: @escaping ([SchoolSATScoresResponseModel]?, Error?) -> Void) {
        if let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") {
            fetchAndDecodeData(from: url, completion: completion)
        } else {
            completion(nil, NetworkingError.invalidUrl)
        }
    }
}

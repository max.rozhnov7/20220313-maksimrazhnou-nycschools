//
//  SchoolResponseModel.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

struct SchoolResponseModel: Decodable {
    let id: String?
    let schoolName: String?
    let email: String?
    let addressLine1: String?
    let city: String?
    let zip: String?
    let state: String?
    let phoneNumber: String?
    let website: String?
    
    let latitude: String?
    let longitude: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case email = "school_email"
        case addressLine1 = "primary_address_line_1"
        case city
        case zip
        case state
        case phoneNumber = "phone_number"
        case website
        case latitude
        case longitude
    }
}

//
//  SchoolSATScoresResponseModel.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

struct SchoolSATScoresResponseModel: Decodable {
    let id: String?
    let schoolName: String?
    let numberOfTestTakers: String?
    
    private let criticalReadingAverage: String?
    private let mathAverage: String?
    private let writingAverage: String?

    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAverage = "sat_critical_reading_avg_score"
        case mathAverage = "sat_math_avg_score"
        case writingAverage = "sat_writing_avg_score"
    }
}

extension SchoolSATScoresResponseModel {
    var criticalReadingAverageScore: Double? {
        get {
            guard let criticalReadingAverage = criticalReadingAverage else {
                return nil
            }
            return Double(criticalReadingAverage)
        }
    }
    
    var mathAverageScore: Double? {
        get {
            guard let mathAverage = mathAverage else {
                return nil
            }
            return Double(mathAverage)
        }
    }
    
    var writingAverageScore: Double? {
        get {
            guard let writingAverage = writingAverage else {
                return nil
            }
            return Double(writingAverage)
        }
    }
}



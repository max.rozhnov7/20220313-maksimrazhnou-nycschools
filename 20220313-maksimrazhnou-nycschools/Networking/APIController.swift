//
//  ApiController.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

import Foundation

class APIController {
    private lazy var jsonDecoder = JSONDecoder()
    
    func fetchAndDecodeData<T: Decodable>(from url: URL, completion: @escaping (T?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  200...299 ~= httpResponse.statusCode else {
                      completion(nil, NetworkingError.invalidStatusCode)
                      return
            }
            
            if let data = data {
                do {
                    let decodedData = try self?.jsonDecoder.decode(T.self, from: data)
                    completion(decodedData, nil)
                } catch {
                    completion(nil, error)
                }
            } else {
                completion(nil, NetworkingError.ivalidData)
            }
        }
        task.resume()
    }
    
    enum NetworkingError: Error {
        case invalidStatusCode
        case ivalidData
        case invalidUrl
    }
}

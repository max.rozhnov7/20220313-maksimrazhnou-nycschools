//
//  SchoolDetailsViewController.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

import UIKit
import MapKit
import MessageUI

/*
 Given more time I would display more info on this VC and would love to make it more interactive
 i.e. being able to open the school's location in the Maps app, to call the school if the phone number is available, etc.
 
 I would also split it further into smaller VCs (sections of information about the school)
 */
class SchoolDetailsViewController: UIViewController {
    
    var school: SchoolModel?
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    @IBOutlet weak var schoolAddressView: UIView!
    @IBOutlet weak var schoolAddressLabel: UILabel!
    
    @IBOutlet weak var schoolEmailView: UIView!
    @IBOutlet weak var schoolEmailButton: UIButton!
    
    @IBOutlet weak var satScoreAveragesStackView: UIStackView!
    
    @IBOutlet weak var noSatScoresView: UIView!
    
    @IBOutlet weak var mathSATScoreLabel: UILabel!
    @IBOutlet weak var readingSATScoreLabel: UILabel!
    @IBOutlet weak var writingSATScoreLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        fillData()
    }
    
    // MARK: - IBActions
    @IBAction func sendEmailDidPress(_ sender: Any) {
        guard let email = school?.email else { return }
        sendEmail(to: email)
    }
    
    // MARK: - Functions
    private func fillData() {
        fillBasicInfo()
        fillSATScores()
        fillMap()
    }
    
    private func fillBasicInfo() {
        schoolNameLabel.text = school?.schoolName
        
        if let email = school?.email {
            schoolEmailButton.setTitle(email, for: [])
        } else {
            schoolEmailView.isHidden = true
        }
        
        if let address = school?.address {
            schoolAddressLabel.text = address
        } else {
            schoolAddressView.isHidden = true
        }
    }
    
    private func fillSATScores() {
        let isSatScoresDataAvailable =
            school?.mathAverageScore != nil &&
            school?.criticalReadingAverageScore != nil &&
            school?.writingAverageScore != nil
        
        satScoreAveragesStackView.isHidden = !isSatScoresDataAvailable
        noSatScoresView.isHidden = isSatScoresDataAvailable
        
        if let mathAverageScore = school?.mathAverageScore {
            mathSATScoreLabel.text = String(mathAverageScore)
        }
        if let readingAverageScore = school?.criticalReadingAverageScore {
            readingSATScoreLabel.text = String(readingAverageScore)
        }
        if let writingAverageScore = school?.writingAverageScore {
            writingSATScoreLabel.text = String(writingAverageScore)
        }
    }
    
    private func fillMap() {
        if let latitudeString = school?.latitude,
           let longitudeString = school?.longitude,
           let latitude = Double(latitudeString),
           let longitude = Double(longitudeString) {
            let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let region = MKCoordinateRegion(center: coordinate,
                                            span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = school?.schoolName
            
            mapView.addAnnotation(annotation)
            mapView.setRegion(region, animated: true)
        } else {
            mapView.isHidden = true
        }
    }
    
    private func setupUI() {
        navigationItem.largeTitleDisplayMode = .never
        
        for subview in satScoreAveragesStackView.arrangedSubviews {
            subview.layer.cornerRadius = 16
        }
        
        mapView.layer.cornerRadius = 16
    }
    
    private func sendEmail(to address: String) {
        //This allows to send an email straight to schools address
        guard MFMailComposeViewController.canSendMail() else { return }

        let composeVC = MFMailComposeViewController()
        composeVC.setToRecipients([address])
        composeVC.mailComposeDelegate = self
        present(composeVC, animated: true)
    }
}

// MARK: - Extension
extension SchoolDetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

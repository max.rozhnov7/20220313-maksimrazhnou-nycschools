//
//  ViewController.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

/*
 Even though I'm quite familiar with common architectures like MVVM, MVP etc., I felt like this little project
 could do with the regular MVC as envisioned by Apple.
 I was reintroduced to this approach at my current workplace and it seems quite reasonable for smaller projects, but it is also
 scalable enough to be used in larger projects.
 */


import UIKit

class SchoolsViewController: UIViewController {

    var schoolsTableViewController: SchoolsTableViewController?
    
    let apiController = SchoolsAPIController()
    let searchController = UISearchController()

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tryAgainButton: UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        fetchData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let schoolsTable = segue.destination as? SchoolsTableViewController {
            schoolsTable.delegate = self
            self.schoolsTableViewController = schoolsTable
        }
    }
    
    // MARK: - IBActions
    @IBAction func tryAgainButtonDidPress(_ sender: UIButton) {
        tryAgainButton.isHidden = true
        fetchData()
    }
    
    // MARK: - Functions
    private func setupUI() {
        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
        
    }
    
    private func fetchData() {
        self.activityIndicator.startAnimating()
        apiController.fetchSchoolsData { [weak self] schools, errors in
            self?.activityIndicator.stopAnimating()
            if let _ = errors {
                self?.tryAgainButton.isHidden = false
                self?.displayErrorAlert()
            } else {
                self?.schoolsTableViewController?.schools = schools ?? []
            }
        }
    }
    
    //Given more time I'd love to do better error handling
    private func displayErrorAlert() {
        let alert = UIAlertController(title: "Sorry", message: "Something went wrong while loading data. Please try again later.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Extensions
extension SchoolsViewController: SchoolsTableViewDelegate {
    func refreshData(completion: (() -> Void)?) {
        apiController.fetchSchoolsData { [weak self] schools, errors in
            if let _ = errors {
                self?.displayErrorAlert()
            } else {
                self?.schoolsTableViewController?.schools = schools ?? []
            }
            completion?()
        }
    }
    
    func didSelectSchool(_ school: SchoolModel) {
        if let schoolDetailsVC = UIStoryboard(name: "SchoolDetails", bundle: nil).instantiateInitialViewController() as? SchoolDetailsViewController {
            schoolDetailsVC.school = school
            self.navigationController?.pushViewController(schoolDetailsVC, animated: true)
        }
    }
}

extension SchoolsViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        schoolsTableViewController?.searchText = searchController.searchBar.text ?? ""
    }
}

//
//  SchoolsTableViewController.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

import UIKit

protocol SchoolsTableViewDelegate: AnyObject {
    func refreshData(completion: (() -> Void)?)
    func didSelectSchool(_ school: SchoolModel)
}

class SchoolsTableViewController: UITableViewController {
    
    var schools: [SchoolModel] = [] {
        didSet {
            schools = schools.filter { $0.schoolName != nil && !($0.schoolName ?? "").isEmpty }
                             .sorted { $0.schoolName! < $1.schoolName! }
            
            tableView.reloadData()
        }
    }
    var searchText: String = "" {
        didSet {
            tableView.reloadData()
        }
    }
    
    weak var delegate: SchoolsTableViewDelegate?
    
    private var schoolsToDisplay: [SchoolModel] {
        get {
            searchText.isEmpty ? schools : schools.filter { $0.schoolName?.lowercased()
                .contains(searchText.lowercased()) ?? false
            }
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //I ussally put such code into extensions as it is used pretty frequently
        let cellName = String(describing: SchoolsTableViewCell.self)
        let nib = UINib(nibName: cellName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellName)
        
        setupUI()
    }
    
    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsToDisplay.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolsTableViewCell.self), for: indexPath) as? SchoolsTableViewCell {
            cell.school = schoolsToDisplay[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        delegate?.didSelectSchool(schoolsToDisplay[indexPath.row])
        return nil
    }
    
    // MARK: - Functions
    private func setupUI() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshContent), for: .valueChanged)
        
        self.refreshControl = refreshControl
    }
    
    @objc private func refreshContent() {
        delegate?.refreshData { [weak self] in
            self?.refreshControl?.endRefreshing()
        }
    }
}

//
//  SchoolsTableViewCell.swift
//  20220313-maksimrazhnou-nycschools
//
//  Created by Maksim Razhnou on 3/13/22.
//

import UIKit

class SchoolsTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    
    var school: SchoolModel? {
        didSet {
            schoolNameLabel.text = school?.schoolName
        }
    }
}
